# Devil create WP

[zx](https://github.com/google/zx) script to create WordPress websites on
mydevil shared hosting with a single command.

## Installation

It looks a little scary, but it's really simple. If someone will ever use it
(despite me of course) and I'll receive requests to create installation script
then I'll consider to create it.

Install nodejs (optional, if you've already did that):

```sh
mkdir ~/.npm-global
npm config set prefix '~/.npm-global'
echo 'export PATH=~/.npm-global/bin:~/bin:$PATH ' >> $HOME/.bash_profile && source $HOME/.bash_profile
mkdir -p ~/bin && ln -fs /usr/local/bin/node15 ~/bin/node && ln -fs /usr/local/bin/npm15 ~/bin/npm && source $HOME/.bash_profile
```

Turn on binexec:

```sh
devil binexec on
```

Clone this repository:

```sh
mkdir -p ~/Downloads
git clone https://gitlab.com/magmast/devil-create-wp ~/Downloads/create-wp
```

Install script and remove cloned repository:

```sh
mkdir -p ~/bin
cp ~/Downloads/create-wp/main.mjs ~/bin/create-wp
echo 'export PATH="$PATH:$HOME/bin" >> ~/.bashrc'
rm -r ~/Downloads/create-wp
```

Install zx:

```sh
npm install -g zx
```

Log out and log in. Now you can use the script as described in the usage
section.

## Usage

```sh
create-wp
```

Later script will ask you about a domain name and a database password.

## License

This project is licensed under GPLv3 license. See [LICENSE](LICENSE) file for
details.
