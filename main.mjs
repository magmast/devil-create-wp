#!/usr/bin/env zx

const domain = await question("Domena: ");

const setWebsiteOptions = async () => {
  await Promise.all([
    $`devil www options ${domain} sslonly on`,
    $`devil www options ${domain} gzip on`,
    $`devil ssl www add 31.186.83.114 le le ${domain}`,
  ]);
};

const installWordPress = async () => {
  const archivePath = path.join(os.homedir(), "tmp/wordpress.tar.gz");

  if (!(await fs.pathExists(archivePath))) {
    await fs.mkdirs(path.dirname(archivePath));
    await $`curl https://wordpress.org/latest.tar.gz -o ${archivePath}`;
  }

  const domainPath = path.join(os.homedir(), "domains", domain);
  cd(domainPath);

  await Promise.all([
    fs.remove(path.resolve(domainPath, "public_html")),
    $`bsdtar -xf ${archivePath}`,
  ]);

  await fs.move(
    path.resolve(domainPath, "wordpress"),
    path.resolve(domainPath, "public_html")
  );
};

const createWebsite = async () => {
  await $`devil www add ${domain} php`;
  await Promise.all([setWebsiteOptions(), installWordPress()]);
};

const getDbName = () => {
  const regex = /[^A-Za-z0-9\-_]/;
  let result = domain;
  while (regex.test(result)) {
    result = result.replace(regex, "");
  }
  return result;
};

const dbname = getDbName();
await Promise.all([createWebsite(), $`devil mysql db add ${dbname} ${dbname}`]);
